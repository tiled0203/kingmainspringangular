package com.example.demo.repositories;

import com.example.demo.domain.Address;
import com.example.demo.domain.BraveKnight;
import com.example.demo.domain.BraveKnightWithInlineAddress;
import com.example.demo.domain.BraveKnightWithoutAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collection;
import java.util.List;

@RepositoryRestResource(excerptProjection = BraveKnightWithInlineAddress.class)
public interface KnightRepository extends PagingAndSortingRepository<BraveKnight, Long>, KnightRepo{
    List<BraveKnight> findBraveKnightByNameLike(String name);

    @Query("select b from BraveKnight b where b.name like %:searchName%")
    List<BraveKnight> findBraveKnightWithNameEndingWithSomething(String searchName);

    List<BraveKnight> findAll();


    List<BraveKnight> findByAddressContaining(Address address);
}
