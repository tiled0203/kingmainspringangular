package com.example.demo.repositories;

import com.example.demo.domain.WebUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WebUserRepository extends JpaRepository<WebUser, Long> {
    WebUser findWebUserByUserName(String username);
}
