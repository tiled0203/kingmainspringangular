package com.example.demo.repositories;

import com.example.demo.domain.Address;
import com.example.demo.domain.BraveKnight;

import java.util.List;

public interface KnightRepo {
    List<BraveKnight> findByAddress(Address address);
}
