package com.example.demo.domain;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.PrintStream;

@Aspect
@Component
@Profile("development")
public class Minstrel {
    private PrintStream stream;

    public Minstrel() {
        this.stream = System.out;
    }

    @Before("execution(* *.embark(..))")
    public void singBeforeQuest() {
        stream.println("Toss a coin to your witcher!");
    }

    @After("execution(* *.embark(..))")
    public void singAfterQuest() {
        stream.println("Tee heehee, the brave knight did embark on a quest!");
    }

    @Around("execution(* *.getName(..))")
    public String getTheNameOfTheKnight(ProceedingJoinPoint proceedingJoinPoint) {
        String name = null;
        try {
            name = (String) proceedingJoinPoint.proceed();
            name = "My name is : " + name;
            System.out.println(name);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return name;
    }

    @Around("execution(* *.testMeOut(..))")
    public String testMe(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        String name = null;
        try {
            Object[] args = proceedingJoinPoint.getArgs();
            args[0] = args[0] + " from the other side!";

            name = (String) proceedingJoinPoint.proceed(args);
            System.out.println(name);
        } catch (Throwable throwable) {
            System.out.println(throwable.getMessage());
            throw throwable;
        }
        return name;
    }

    @Before("execution(* *.testMeOut(String)) && args(theName)")
    public void printOutName(String theName) {
        System.out.println("------" + theName + "-----");
    }

}