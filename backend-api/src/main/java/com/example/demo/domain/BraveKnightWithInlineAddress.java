package com.example.demo.domain;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "inlineAddress", types = {BraveKnight.class})
public interface BraveKnightWithInlineAddress {
    Long getId();
    String getName();
    Address getAddress();
}
