package com.example.demo.domain;


import javax.persistence.*;
import java.util.List;

@Entity
public class WebUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String userName;
    private String password;
    @OneToMany(cascade = CascadeType.ALL )
    private List<WebUserAuthority> userAuthorityList;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<WebUserAuthority> getUserAuthorityList() {
        return userAuthorityList;
    }

    public void setUserAuthorityList(List<WebUserAuthority> userAuthorityList) {
        this.userAuthorityList = userAuthorityList;
    }
}
