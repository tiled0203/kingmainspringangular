package com.example.demo.domain;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "noAddress", types = {BraveKnight.class})
public interface BraveKnightWithoutAddress {
    String getName();
    String getCreatedBy();
}
