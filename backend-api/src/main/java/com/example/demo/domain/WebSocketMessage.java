package com.example.demo.domain;

public class WebSocketMessage {
    public enum Type {
        SERVER, MESSAGE
    }

    private String message;
    private Type type = Type.SERVER;

    public WebSocketMessage() {
    }

    public WebSocketMessage(String message) {
        this.message = message;
    }

    public WebSocketMessage(String message, Type type) {
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
