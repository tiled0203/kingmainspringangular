package com.example.demo.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class BraveKnight implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    private List<Address> address;

    @ManyToOne
    private Quest quest;
    private String name;

    private String createdBy;

    public BraveKnight() {
    }

    public BraveKnight(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public Quest getQuest() {
        return quest;
    }

    public void setQuest(Quest quest) {
        this.quest = quest;
    }

    public String getName() {
        return name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
