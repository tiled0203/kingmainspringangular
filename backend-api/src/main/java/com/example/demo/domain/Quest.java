package com.example.demo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Quest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    public Quest(String description) {
        this.description = description;
    }

    public Quest() {
        
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
