package com.example.demo;

import com.example.demo.domain.Address;
import com.example.demo.domain.BraveKnight;
import com.example.demo.repositories.KnightRepository;
import com.example.demo.services.KnightService;
import com.github.javafaker.Faker;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronTrigger;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;

import static java.lang.Thread.sleep;


@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private KnightRepository knightRepository;
    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private SpringTemplateEngine thymeleaf;

    public static void main(String[] args) throws InterruptedException {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(Application.class).run(args);
    }


    @Override
    public void run(String... args) throws Exception {
        String users[] = new String[]{"timothy", "admin"};
        for (int i = 0; i < 100; i++) {
            BraveKnight braveKnight = new BraveKnight(new Faker().funnyName().name());
            braveKnight.setCreatedBy(users[i % 2]);
            knightRepository.save(braveKnight);
        }
    }

//    @Scheduled(cron = "0 * * * * *")
    public void logSomething() {
        System.out.println("sending email");
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            Context ctx = new Context();
            ctx.setVariable("name", "Timothy");
            ctx.setVariable("text", "What's Up Doc?----------");
            ctx.setVariable("logo", "realdolmenLogo");

            String emailText = thymeleaf.process("email", ctx);
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            FileSystemResource file = new FileSystemResource("backend-api/src/main/resources/import.sql");
            helper.addAttachment("import.sql", file);
            helper.setTo("tim.developer01@gmail.com");
            helper.setFrom("example@mail.com");
            helper.setSubject("Spring mail");
            helper.setText(emailText, true);
            ClassPathResource imageSource = new ClassPathResource("static/realdolmenLogo.png");
            helper.addInline("realdolmenLogo", imageSource,"image/png" );

            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }


}
