package com.example.demo.config;

import com.example.demo.domain.WebUser;
import com.example.demo.domain.WebUserAuthority;
import com.example.demo.repositories.WebUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;
import java.util.Arrays;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class KingdomSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private WebUserRepository webUserRepository;

    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("timothy").password(encoder().encode("12345678")).roles("USER")
                .and().withUser("admin").password(encoder().encode("12345678")).roles("ADMIN");
//        auth.jdbcAuthentication()
//                .usersByUsernameQuery("select user_name, password , true from web_user where user_name = ?")
//                .authoritiesByUsernameQuery("select user_name, authority from web_user_authority where user_name = ?")
//                .dataSource(dataSource).passwordEncoder(encoder());
//        auth.userDetailsService(new WebUserService(webUserRepository));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.anonymous().disable()
                .authorizeRequests()
                .antMatchers("/knight/all").authenticated()
                .anyRequest().permitAll()
                .and()
                .httpBasic()
                .and().logout().logoutUrl("/logout").and()
        .csrf().disable()
//                .and().csrf().csrfTokenRepository(getCsrfTokenRepository())
        ;
    }

    private CsrfTokenRepository getCsrfTokenRepository() {
        CookieCsrfTokenRepository tokenRepository = CookieCsrfTokenRepository.withHttpOnlyFalse();
        tokenRepository.setCookiePath("/");
        return tokenRepository;
    }

    @Bean
    public void test() {
        WebUser webUser = new WebUser();
        WebUserAuthority user = new WebUserAuthority();
        user.setAuthority("USER");
        user.setUserName("timothy");
        WebUserAuthority admin = new WebUserAuthority();
        admin.setAuthority("ADMIN");
        admin.setUserName("timothy");
        webUser.setPassword(encoder().encode("12345678"));
        webUser.setUserName("timothy");
        webUser.setUserAuthorityList(Arrays.asList(user, admin));
        webUserRepository.save(webUser);
        System.out.println(encoder().encode("12345678"));
    }
}
//$2a$10$RRfu/o4mDFxGC1XULMRJfuX2dOeHFg8mycrEF.JLBYK6uUb7eJ8F2