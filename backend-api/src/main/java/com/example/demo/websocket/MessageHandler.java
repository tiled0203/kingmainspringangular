package com.example.demo.websocket;

import com.example.demo.domain.WebSocketMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

@Controller
public class MessageHandler {

    @Autowired
    private SimpMessagingTemplate template;

    @MessageMapping("/greeting")
    @SendTo("/topic/response")
    public WebSocketMessage handle(WebSocketMessage message) {
        try {
            Thread.sleep(1000); // simulated delay
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new WebSocketMessage("Hello " + HtmlUtils.htmlEscape(message.getMessage()) + ", how are you?", WebSocketMessage.Type.MESSAGE);
    }

    public void send(WebSocketMessage webSocketMessage) {
        WebSocketMessage message = new WebSocketMessage("Message from server: " + HtmlUtils.htmlEscape(webSocketMessage.getMessage()), WebSocketMessage.Type.SERVER);
        this.template.convertAndSend("/topic/response", message);
    }
}
