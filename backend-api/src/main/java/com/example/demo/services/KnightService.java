package com.example.demo.services;

import com.example.demo.domain.Address;
import com.example.demo.domain.BraveKnight;
import com.example.demo.domain.WebSocketMessage;
import com.example.demo.repositories.AddressRepository;
import com.example.demo.repositories.KnightRepository;
import com.example.demo.websocket.MessageHandler;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class KnightService {
    private KnightRepository knightRepository;
    private AddressRepository addressRepository;
    private MessageHandler messageHandler;

    public KnightService(KnightRepository knightRepository, AddressRepository addressRepository, MessageHandler messageHandler) {
        this.knightRepository = knightRepository;
        this.addressRepository = addressRepository;
        this.messageHandler = messageHandler;
    }

    //    @Transactional
    @PreAuthorize("hasAnyRole({'ROLE_ADMIN', 'ROLE_USER'})")
//    @PostFilter("hasRole('ROLE_ADMIN') || filterObject.createdBy == principal.username")
    public Page<BraveKnight> getKnights(Pageable pageable) {
        Page<BraveKnight> all = knightRepository.findAll(pageable);
        return all;
    }

    public List<BraveKnight> getKnightsWithoutPage() {
        List<BraveKnight> all = knightRepository.findAll();
        return all;
    }

    public List<BraveKnight> getKnightsByName(String name) {
        return knightRepository.findBraveKnightByNameLike(name);
    }

    public List<BraveKnight> getKnightsByNameContaining(String name) {
        return knightRepository.findBraveKnightWithNameEndingWithSomething(name);
    }

    //    @Transactional(propagation = Propagation.NEVER)
//    @Secured({"ROLE_ADMIN"})
    public List<BraveKnight> getKnightsByAddress(Address address) {
        List<BraveKnight> byAddress = knightRepository.findByAddress(address);
//        byAddress.get(0).getAddress().size();
//        Hibernate.initialize(byAddress.get(0).getAddress());
        return byAddress;
    }

    public BraveKnight save(BraveKnight braveKnight) {
        knightRepository.save(braveKnight);
        messageHandler.send(new WebSocketMessage("A new knight with name:" + braveKnight.getName() + " is created"));
        return braveKnight;
    }

    public BraveKnight findKnightById(Long id) throws NotFoundException {
        Optional<BraveKnight> knight = knightRepository.findById(id);
        return knight.orElseThrow(() -> new NotFoundException("knight is not found"));
    }

    public List<BraveKnight> findKnightByStreetAddress(String street) {
        Address addressByStreet = addressRepository.findAddressByStreet(street);
        return knightRepository.findByAddressContaining(addressByStreet);
    }
}
