package com.example.demo.restcontroller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class WebUserRestController {
//    private Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    @GetMapping("")
    public Authentication getAuth(){
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
