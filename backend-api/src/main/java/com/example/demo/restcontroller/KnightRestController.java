package com.example.demo.restcontroller;

import com.example.demo.domain.BraveKnight;
import com.example.demo.services.KnightService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("knight")  // http://localhost:8080/api/knight
public class KnightRestController {

    @Autowired
    private KnightService knightService;

    @GetMapping("/all") // http://localhost:8080/api/knight/all
//    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Page<BraveKnight> getAll(Pageable pageable) {
        return knightService.getKnights(pageable);
    }

    @GetMapping("/all/nopaging")
    public List<BraveKnight> findAllWithoutPaging() {
        return knightService.getKnightsWithoutPage();
    }

    @GetMapping("/find/{idknight}")
    public BraveKnight findById(@PathVariable("idknight") Long id) throws NotFoundException {
        return knightService.findKnightById(id);
    }

    @GetMapping("/find") //http://localhost:8080/api/knight/find?name=....&street=....
    public List<BraveKnight> findByName(@RequestParam("name") String name, @RequestParam(value = "street", required = false) String street){
        if(street == null){
            return knightService.getKnightsByNameContaining(name);
        }else{
            return knightService.findKnightByStreetAddress(street);
        }
    }

    @PostMapping("") //http://localhost:8080/api/knight
    public BraveKnight create(@RequestBody BraveKnight braveKnight){
        return knightService.save(braveKnight);
    }


}