package com.example.demo.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@RestController
@RequestMapping("movie")
public class MovieRestController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("")
    public ResponseEntity<ArrayList> getMovie(){
        return restTemplate.getForEntity("https://angularcoursebackend.azurewebsites.net/api/movies" , ArrayList.class);
    }
}
