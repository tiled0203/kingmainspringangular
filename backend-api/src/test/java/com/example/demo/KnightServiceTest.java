package com.example.demo;

import com.example.demo.domain.Address;
import com.example.demo.domain.BraveKnight;
import com.example.demo.repositories.KnightRepository;
import com.example.demo.services.KnightService;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
//@RunWith(MockitoJUnitRunner.class)
public class KnightServiceTest {
    @InjectMocks
    private KnightService knightService;
    @Mock
    private KnightRepository knightRepository;

    Address address;

    @Captor
    ArgumentCaptor<Address> addressArgumentCaptor;
    private BraveKnight timothy;

    @BeforeEach // junit 5
//    @Before // junit 4
    public void setUp() throws Exception {
        List<BraveKnight> braveKnightList = new ArrayList<>();
        braveKnightList.add(new BraveKnight("knight1"));
        braveKnightList.add(new BraveKnight("knight2"));
        Mockito.lenient().when(knightRepository.findAll()).thenReturn(braveKnightList);
        address = new Address("My street", "5A", "City");
        address.setId(5L);

        timothy = new BraveKnight("Timothy");
        timothy.setAddress(Arrays.asList(address));
        Mockito.lenient().when(knightRepository.findByAddress(address)).thenReturn(Arrays.asList(timothy));

    }

    @Test
    @DisplayName(value = "This test will use a fake knightrepo and verify that findAll is being called once")
    public void testWithFakeRepo() {
        knightService.getKnightsWithoutPage();
        Mockito.verify(knightRepository).findAll();
        Mockito.verifyNoMoreInteractions(knightRepository);
    }

    @Test
    public void testWithFakeKnightData() {
        List<BraveKnight> knights = knightService.getKnightsWithoutPage();
        Assert.assertEquals("knight1", knights.get(0).getName());
        Assert.assertEquals("knight2", knights.get(1).getName());
    }

    @Test
    public void testFindByAddress() {
        List<BraveKnight> knightsByAddress = knightService.getKnightsByAddress(address);

        Mockito.verify(knightRepository).findByAddress(addressArgumentCaptor.capture());
        Assertions.assertNotNull(addressArgumentCaptor.getValue());
        Assertions.assertFalse(knightsByAddress.isEmpty());

        Assertions.assertAll(
                () -> Assertions.assertEquals(5L, addressArgumentCaptor.getValue().getId()),
                () -> Assertions.assertEquals("Timothy", knightsByAddress.get(0).getName()),
                () -> Assertions.assertEquals("My street", knightsByAddress.get(0).getAddress().get(0).getStreet()),
                () -> Assertions.assertEquals("5A", knightsByAddress.get(0).getAddress().get(0).getHouseNumber()),
                () -> Assertions.assertEquals("City", knightsByAddress.get(0).getAddress().get(0).getCity())
        );
        Mockito.verifyNoMoreInteractions(knightRepository);
    }
}
