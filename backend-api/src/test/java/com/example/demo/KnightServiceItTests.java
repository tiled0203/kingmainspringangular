package com.example.demo;

import com.example.demo.domain.Address;
import com.example.demo.domain.BraveKnight;
import com.example.demo.services.KnightService;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

//@RunWith(SpringJUnit4ClassRunner.class)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = Application.class)
//@ActiveProfiles(profiles = "test")
@SpringBootTest
public class KnightServiceItTests {

    @Autowired
    private KnightService knightService;

    @Test
    public void testThatWeHaveAKnight() {
        Address address = new Address();
        address.setId(1L);
        List<BraveKnight> knightsByAddress = knightService.getKnightsByAddress(address);
//        Assert.assertFalse(knightsByAddress.isEmpty());
//        Assert.assertFalse(knightsByAddress.get(0).getName().isEmpty());

        Assertions
                .assertAll(
                        () -> Assertions.assertFalse(knightsByAddress.isEmpty()),
                        () -> Assertions.assertFalse(knightsByAddress.get(0).getName().isEmpty())
                );


    }


}
