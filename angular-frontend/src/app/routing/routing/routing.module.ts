import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {KnightTableComponent} from "../../components/knights/knight-table/knight-table.component";
import {KnightAddComponent} from "../../components/knights/knight-add/knight-add.component";
import {QuestListComponent} from "../../components/quests/quest-list/quest-list.component";
import {QuestAddComponent} from "../../components/quests/quest-add/quest-add.component";
import {KingdomComponent} from "../../pages/kingdom/kingdom.component";
import {AuthGuard} from "../../services/auth-guard.service";
import {LoginComponent} from "../../components/login/login.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'kingdom', component: KingdomComponent},
  {path: 'knight/list', component: KnightTableComponent, canActivate: [AuthGuard]},
  {path: 'knight/add', component: KnightAddComponent, canActivate: [AuthGuard]},
  {path: 'quest/list', component: QuestListComponent, canActivate: [AuthGuard]},
  {path: 'quest/add', component: QuestAddComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: 'kingdom', pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class RoutingModule {
}
