import {Component, OnInit} from '@angular/core';
import {Message, Type, WebSocketAPI} from "../../websocket/WebSocketApi";

@Component({
  selector: 'app-kingdom',
  templateUrl: './kingdom.component.html',
  styleUrls: ['./kingdom.component.css']
})
export class KingdomComponent implements OnInit {
  messages: Message[] = [];
  messageSend: Message | undefined;

  constructor(private webSocketAPI: WebSocketAPI) {
    this.webSocketAPI.messages.subscribe(value => {
      if (value.type === Type.MESSAGE) {
        this.messages.push(value);
      }
    });
  }

  ngOnInit(): void {
  }

  sendMessage() {
    console.log("new message from client to websocket: ", this.messageSend);
    this.webSocketAPI.send(this.messageSend);
  }

}
