import {Component, OnInit} from '@angular/core';
import {Knight} from "../../../model/Knight";
import {KnightService} from "../../../services/knight.service";
import {Page} from "../../../model/Page";

@Component({
  selector: 'app-knight-table',
  templateUrl: './knight-table.component.html',
  styleUrls: ['./knight-table.component.css']
})
export class KnightTableComponent implements OnInit {

  page: Page<Knight> | undefined;
  currentPage: number = 0;

  constructor(private knightService: KnightService) {
  }

  ngOnInit(): void {
    this.getPage(0);
  }

  getPage(page: number) {
    this.currentPage = page;
    this.knightService.getKnights(this.currentPage).subscribe(value => {
      console.log(value)
      this.page = value
    });
  }
}
