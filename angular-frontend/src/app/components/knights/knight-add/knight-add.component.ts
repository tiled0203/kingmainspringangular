import { Component, OnInit } from '@angular/core';
import {KnightService} from "../../../services/knight.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-knight-add',
  templateUrl: './knight-add.component.html',
  styleUrls: ['./knight-add.component.css']
})
export class KnightAddComponent implements OnInit {

  constructor(private knightService:KnightService) { }

  ngOnInit(): void {
  }

  add(addKnightForm: NgForm) {
    this.knightService.createKnight(addKnightForm.value).subscribe();
  }
}
