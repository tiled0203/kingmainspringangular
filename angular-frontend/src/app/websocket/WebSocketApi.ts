// @ts-ignore
import * as SockJS from 'sockjs-client';
import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {Stomp} from "@stomp/stompjs";

export interface Message {
  message: string;
  type: Type;
}

export enum Type {
  SERVER = 'SERVER',
  MESSAGE = 'MESSAGE'
}

@Injectable()
export class WebSocketAPI {
  private webSocketEndPoint: string = '/ws';
  private topic: string = "/topic/response";
  private stompClient: any;
  private _messages: Subject<Message> = new Subject();
  public messages = this._messages.asObservable();

  constructor() {
  }

  connect() {
    console.log("Initialize WebSocket Connection");

    const socket = new SockJS(this.webSocketEndPoint);
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, () => {
      this.stompClient.subscribe(this.topic, (sdkEvent: any) => {
        this.onMessageReceived(JSON.parse(sdkEvent.body));
      });
      this.stompClient.reconnect_delay = 2000;
    }, this.errorCallBack);
  }

  disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
    console.log("Disconnected");
  }

  // on error, schedule a reconnection attempt
  private errorCallBack(error: any) {
    console.log("errorCallBack -> " + error)
    setTimeout(() => {
      this.connect();
    }, 5000);
  }

  send(message: Message | undefined) {
    this.stompClient.send("/app/greeting", {}, JSON.stringify(message));
  }

  private onMessageReceived(received: Message) {
    console.log(received.type + " Message Received from Server :: " + received.message);
    this._messages.next(received);
  }
}
