import {Injectable} from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest, HttpResponse,
  HttpXsrfTokenExtractor
} from "@angular/common/http";
import {LoginService} from "./login.service";
import {pipe} from "rxjs";
import {tap} from "rxjs/operators";


@Injectable()
export class XhrInterceptor implements HttpInterceptor {

  constructor(private tokenExtractor: HttpXsrfTokenExtractor) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let token = sessionStorage.getItem('token');
    let authReq = req.clone({
      setHeaders: {
        'X-Requested-With': 'XMLHttpRequest',
        'authorization': token ? token : ''
      }
    });

    return next.handle(authReq);
  }
}
