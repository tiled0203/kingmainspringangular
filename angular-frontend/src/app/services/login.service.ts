import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private authenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private user: Subject<string> = new Subject<string>()
  static authToken = '';
  private return: any;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
    this.route.queryParams
      .subscribe(params => this.return = params['return'] || '/');
  }

  login(password: string, username: string) {
    LoginService.authToken = 'Basic ' + btoa(username + ':' + password);
    const headers = new HttpHeaders(password ? {
      authorization: LoginService.authToken
    } : {});
    sessionStorage.setItem('token', password ? LoginService.authToken : '');

    this.http.get<any>('/user', {headers: headers}).subscribe(response => {
      if (response) {
        // @ts-ignore
        this.user.next(response.name);
        this.authenticated.next(true);
        this.router.navigateByUrl(this.return);
      } else {
        this.user.next('');
        this.authenticated.next(false);
      }
    });
  }

  logout() {
    this.http.post<any>('/logout', {}).subscribe(response => {
      this.user.next('');
      this.authenticated.next(false);
      sessionStorage.setItem("token", '');
      this.router.navigate(['/']);
    });
  }

  isAuthenticated(): Promise<boolean> {
    return new Promise((resolve) => {
        this.http.get<any>('/user').subscribe(response => {
          if (response) {
            // @ts-ignore
            this.user.next(response.name);
            this.authenticated.next(true);
            resolve(true);
          } else {
            this.user.next('');
            this.authenticated.next(false);
            resolve(false);
          }
        }, error => {
          this.user.next('');
          this.authenticated.next(false);
          resolve(false);
        })
      }
    )
  }

  getUser(): Observable<string> {
    return this.user.asObservable();
  }
}
