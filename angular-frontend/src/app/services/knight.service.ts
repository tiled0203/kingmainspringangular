import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Knight} from "../model/Knight";
import {Observable} from "rxjs";
import {Page} from "../model/Page";

@Injectable({
  providedIn: 'root'
})
export class KnightService {

  constructor(private http: HttpClient) {
  }

  getKnights(page: number): Observable<Page<Knight>> {
    return this.http.get<Page<Knight>>("/knight/all", {
      params: {
        page: page,
        size: 20,
      }
    });
  }

  createKnight(value: Knight) {
    return this.http.post<Knight>("/knight", value);

  }
}
