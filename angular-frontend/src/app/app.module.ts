import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule} from "@angular/common/http";
import {HeaderComponent} from './components/header/header.component';
import {KnightTableComponent} from './components/knights/knight-table/knight-table.component';
import {KnightAddComponent} from './components/knights/knight-add/knight-add.component';
import {QuestAddComponent} from './components/quests/quest-add/quest-add.component';
import {KingdomComponent} from './pages/kingdom/kingdom.component';
import {RouterModule} from "@angular/router";
import {QuestListComponent} from './components/quests/quest-list/quest-list.component';
import {RoutingModule} from "./routing/routing/routing.module";
import {LoginComponent} from './components/login/login.component';
import {FormsModule} from "@angular/forms";
import {XhrInterceptor} from "./services/xhr-interceptor.service";
import {WebSocketAPI} from "./websocket/WebSocketApi";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastComponent } from './components/toast/toast.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    KnightTableComponent,
    KnightAddComponent,
    QuestAddComponent,
    QuestListComponent,
    KingdomComponent,
    LoginComponent,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RoutingModule,
    FormsModule,
    NgbModule,

  ],
  providers: [WebSocketAPI,{provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
