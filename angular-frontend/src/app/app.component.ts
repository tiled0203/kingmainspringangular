import {Component, OnInit} from '@angular/core';
import {Message, Type, WebSocketAPI} from "./websocket/WebSocketApi";
import {ToastService} from "./services/toast.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-frontend';

  serverMessages: Message[] = [];

  constructor(private webSocketAPI: WebSocketAPI, private toastService: ToastService) {
  }

  ngOnInit(): void {
    this.webSocketAPI.connect();
    this.webSocketAPI.messages.subscribe(value => {
      if (value.type === Type.SERVER) {
        this.toastService.show(value.message,{ classname: 'bg-success text-light', delay: 10000 });
      }
    });
  }
}
